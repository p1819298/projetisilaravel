@extends('layouts.layout')

@section('title')
    Vous
@endsection

@section('correction_url')../@endsection

@section('page_contenu')
@if(session()->has('info'))
<div class="card text-white bg-danger mb-3 mx-auto" style="max-width: 18rem;">
<div class="card-body">
<p class="card-text">{{ session('info') }}</p>
</div>
</div>
@endif
<div class="">
    <div class="text-center mt-3">
        <?php 
        $indImage = rand(1,5);
        
        ?>
        <img class="rounded-circle bg-secondary" width="200" height="200" src=<?=asset('assets/img/iconesPdp/pdp'.$indImage.
        '.jpg')?>/>
        <p class="display-4 text-white">{{$util->prenom}} {{$util->nom}}</p>
        <p class="text-white">{{$util->email}}</p>
    </div>

    <div class="text-center mt-3">
        <div class="d-flex justify-content-center">
            <div>
                <a class="px-2 text-white" class="choixContent" id="btnImageAimer" href="#" onclick="return false;">Aimées</a>
                <hr id="imageAimerSouligne" style=" border-top:3px solid white;">
            </div>
            <div>
                <a class="px-2 text-white" class="choixContent" id="btnImageCree" href="#" onclick="return false;">Crées</a>
                <hr id="imageCreeSouligne" style=" border-top:3px solid white;">
            </div>

            
        </div>

        

        <div id="images_crees">

            @if( $util->id == Auth::user()->id)
            <a class="text-center btn btn-success" href="{{route('images.create')}}">Ajouter une nouvelle image</a>
            @endif
            <?php $i=1; ?>
            <?php $nbImages=0;?>
            <div class="pin_container">
            @foreach($util->imagesCrees as $image)
            <?php
                
                if($i==1)
                {
                    
                    echo '<a href="'.route('images.show',$image->id).'" data="'.$nbImages.'" class="card card_small cardCree">';
                    $i = $i+1;
                }
                else if($i==2)
                {
                    echo '<a href="'.route('images.show',$image->id).'" data="'.$nbImages.'" class="card card_medium cardCree">';
                    $i = $i+1;
                }
                else
                {
                    echo '<a href="'.route('images.show',$image->id).'" data="'.$nbImages.'" class="card card_large cardCree">';
                    $i = 1;
                }
                
                
            ?>
                <img class="card-img-top rounded" width="400" height="500" src="{{ asset($image->source) }}" alt="">
                @if( $util->id == Auth::user()->id)
                <form action="{{route('images.destroy',$image->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                <button id=<?='boutonC'.$nbImages ?> class="bouton_supprimer btn btn-danger"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
  <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293 5.354 4.646z"/>
</svg></button>
                 </form>

                <form action="{{route('images.edit',$image->id)}}">
                    @csrf
                <button id=<?='boutonS'.$nbImages ?> class="bouton_maj btn btn-secondary"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
</svg></button>
                </form>
                @endif
                <?php $nbImages = $nbImages+1?>
                
            </a>
            @endforeach
            </div>  
        </div>


        <div id="images_aimees">
            <?php $i=1; ?>
            <?php $nbImages=0;?>
            <div class="pin_container">
            @foreach($util->imagesAimees as $image)
            <?php
                
                if($i==1)
                {
                    
                    echo '<a href="'.route('images.show',$image->id).'" data="'.$nbImages.'" class="card card_small cardAimer">';
                    $i = $i+1;
                }
                else if($i==2)
                {
                    echo '<a href="'.route('images.show',$image->id).'" data="'.$nbImages.'" class="card card_medium cardAimer">';
                    $i = $i+1;
                }
                else
                {
                    echo '<a href="'.route('images.show',$image->id).'" data="'.$nbImages.'" class="card card_large cardAimer">';
                    $i = 1;
                }
                
                
            ?>
                <img class="card-img-top rounded" width="400" height="500" src="{{ asset($image->source) }}" alt="">
                <?php $nbImages = $nbImages+1?>
                
            </a>
            @endforeach
            </div>  
        </div>

        

        

        

        
    </div>


</div>

@endsection

@section('script')
        //Section sélectionnée
        var images_crees = document.getElementById("images_crees");
        var images_aimees = document.getElementById("images_aimees");

       


        var imageCreeSouligne = document.getElementById("imageCreeSouligne");
        var imageAimerSouligne = document.getElementById("imageAimerSouligne");

        var btnImageAimerContent = function() {
            images_crees.style.display="none";
            images_aimees.style.display="contents";
            imageCreeSouligne.style.visibility="hidden";
            imageAimerSouligne.style.visibility="visible";
        };

        var btnImageCreeContent = function() {
            images_crees.style.display="contents";
            images_aimees.style.display="none";
            imageCreeSouligne.style.visibility="visible";
            imageAimerSouligne.style.visibility="hidden";
        };

        document.getElementById("btnImageAimer").addEventListener('click',btnImageAimerContent,false);
        document.getElementById("btnImageCree").addEventListener('click',btnImageCreeContent,false);




        //Boutons apparitions
        var cardAimer = document.getElementsByClassName("cardAimer");
        var cardCrees = document.getElementsByClassName("cardCree");
        var classbutton = document.getElementsByClassName("bouton_aimer");

        //Fonctions qui cachent ou font apparaître des boutons
        var visibleAimer = function() {
            var attribute = this.getAttribute("data");
            var button = document.getElementById("boutonA"+attribute);
            button.style.visibility = 'visible';
        };

        var cacherAimer = function() {
            var attribute = this.getAttribute("data");
            var button = document.getElementById("boutonA"+attribute);
            button.style.visibility = 'hidden';
        };

        var visibleCree = function() {
            var attribute = this.getAttribute("data");
            var button = document.getElementById("boutonC"+attribute);
            var buttonS = document.getElementById("boutonS"+attribute);
            button.style.visibility = 'visible';
            buttonS.style.visibility = 'visible';
        };

        var cacherCree = function() {
            var attribute = this.getAttribute("data");
            var button = document.getElementById("boutonC"+attribute);
            var buttonS = document.getElementById("boutonS"+attribute);
            button.style.visibility = 'hidden';
            buttonS.style.visibility = 'hidden';
        };


        //Initialisation des évènements pour les boutons
        /*for (var i = 0; i < cardAimer.length; i++) {
            cardAimer[i].addEventListener('mouseover', visibleAimer, false);
        }

        for (var i = 0; i < cardAimer.length; i++) {
            cardAimer[i].addEventListener('mouseout', cacherAimer, false);
        }*/

        for (var i = 0; i < cardCrees.length; i++) {
            cardCrees[i].addEventListener('mouseover', visibleCree, false);
        }

        for (var i = 0; i < cardCrees.length; i++) {
            cardCrees[i].addEventListener('mouseout', cacherCree, false);
        }
@endsection