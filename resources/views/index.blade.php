<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{asset('assets/css/accueil.css')}}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>AnimalsHub - France</title>
</head>
<body style="overflow: hidden;" class="bg-dark">
    <div>
        <nav class="navbar navbar-expand-lg bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand text-light " href="#">AnimalsHub</a>
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 sm:block">
                    @auth
                        <a href="{{ route('images.index') }}" class="text-sm text-gray-700 dark:text-gray-500 btn btn-success rounded-pill underline">Aller sur le site</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 btn btn-danger rounded-pill underline">Se connecter</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline btn btn-light rounded-pill">S'inscrire</a>
                        @endif
                    @endauth
                </div>
            @endif
            </div>
        </div>  

        </nav>

        <div class="bd-example bg-light mt-2 bg-dark">
        <div class="mx-auto text-center">
            <img width="150" height="150" class="rounded-circle text-center" src="{{asset('assets/img/iconesPdp/logo2.png')}}"/>
        </div>
            <p class="text-center display-5 text-white">Un site pour les amis des animaux</p>
            <p class="text-center display-5 text-white">+ de 750 000 images</p>
            <div class="d-flex justify-content-around">
            <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active text-center">
                    <img class="rounded" src="{{asset('assets/img/fond/bouledogue.jpg')}}" height="300" width="300"/>
                    <img class="rounded" src="{{asset('assets/img/fond/p.jpg')}}" height="300" width="300"/>               
                    </div>
                    <div class="carousel-item text-center">
                    <img class="rounded" src="{{asset('assets/img/fond/pigeon.jpg')}}" height="300" width="300"/>
                    <img class="rounded" src="{{asset('assets/img/fond/colombe.jpg')}}" height="300" width="300"/>
                    
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only"></span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only"></span>
                </a>
                </div>
            </div>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    
</body>
</html>