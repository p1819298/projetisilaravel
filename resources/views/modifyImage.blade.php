@extends('layouts.layout')

@section('title')
    Modifier une image
@endsection

@section('correction_url')../../@endsection


@section('page_contenu')
@if(session()->has('info'))
<div class="card text-white bg-success mb-3 mx-auto" style="max-width: 18rem;">
<div class="card-body">
<p class="card-text">{{ session('info') }}</p>
</div>
</div>
@endif
<div class="container">
    <div class="row shadow card text-black bg-dark w-50 mx-auto p-3"  style="background-color:rgb(27,30,31);">
        <p class="text-center display-5">Modifiez votre image</p>  
        <form action="{{route('images.update',$image->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group mt-4">
                <label for="title" class="form-label col-md-5 col-10">Titre de l'image</label>
                <input id="title" class="border form-control @error('title') is-invalid @enderror col-md-3 shadow col-6 text-light" style="background-color:rgb(24,26,27);border:1px solid rgb(50,54,56);" value="{{old('title',$image->title)}}"  name="title" type="text" placeholder="Mon animal preferé"/>
                @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group row mt-4">
                <label for="description" class="form-label col-md-5 col-10">Description</label>
                <textarea id="description" class="border form-control  @error('description') is-invalid @enderror col-md-3 shadow col-6 text-light" style="background-color:rgb(24,26,27);border:1px solid rgb(50,54,56);" value=""  name="description" type="textarea" placeholder="Je l'aime tellement">{{old('description',$image->description)}}</textarea>
                @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group row mt-4">
            <label for="id_genre" class="form-label col-md-5 col-10">Genre</label>
            <select name="id_genre" name="id_genre" value="{{old('id_genre',$image->id_genre)}}" id="id_genre" style="background-color:rgb(24,26,27);border:1px solid rgb(50,54,56);" class="text-light form-select form-control @error('id_genre') is-
invalid @enderror">
            @foreach($listeGenre as $genre)
                    <option class="text-light" value="{{$genre->id}}" {{ ( $genre->id == old('id_genre',$image->id_genre)) ? 'selected' : '' }}>{{$genre->genreName}}</option>
            @endforeach
            </select>
            
            </div>

            <div class="form-group row mt-4">
                <label for="source" name="source" class="label-file">Choisir une image</label>
                <input id="source" name="source" class="form-control @error('source') is-invalid @enderror input-file mt-3" value="{{old('source',$image->source)}}" type="file">
                @error('source')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="justify-content-end mt-4 text-center">
                <button type="submit" class="btn btn-success">Publier !</button>
            </div>
        </form>
    </div>
</div>
@endsection

