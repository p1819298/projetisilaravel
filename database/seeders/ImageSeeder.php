<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        //Felins
        for($i = 1; $i <= 15; $i++) 
        {
            $randUtil = rand(1,10);
            \App\Models\Image::create([
                'title' => 'felin'.$i,
                'description' => $faker->sentence(10,true),
                'user_id'=>$randUtil,
                'id_genre'=>1,
                'source' => '/assets/img/contenu_site/felin'.$i.'.jpg',
                'date_creation' => date("01-03-21")
            ]);
        }

        //Reptiles
        for($i = 1; $i <= 15; $i++) 
        {
            $randUtil = rand(1,10);
            \App\Models\Image::create([
                'title' => 'reptile'.$i,
                'description' => $faker->sentence(10,true),
                'user_id'=>$randUtil,
                'id_genre'=>2,
                'source' => '/assets/img/contenu_site/reptile'.$i.'.jpg',
                'date_creation' => date("05-04-22")
            ]);
        }

        //Oiseaux
        for($i = 1; $i <= 15; $i++) 
        {
            $randUtil = rand(1,10);
            \App\Models\Image::create([
                'title' => 'oiseau'.$i,
                'description' => $faker->sentence(10,true),
                'user_id'=>$randUtil,
                'id_genre'=>3,
                'source' => '/assets/img/contenu_site/bird'.$i.'.jpg',
                'date_creation' => date("30-05-22")
            ]);
        }

        //Poissons
        for($i = 1; $i <= 15; $i++) 
        {
            $randUtil = rand(1,10);
            \App\Models\Image::create([
                'title' => 'poisson'.$i,
                'description' => $faker->sentence(10,true),
                'user_id'=>$randUtil,
                'id_genre'=>4,
                'source' => '/assets/img/contenu_site/fish'.$i.'.jpg',
                'date_creation' => date("05-05-22")
            ]);
        }

        //Animaux de la ferme
        for($i = 1; $i <= 15; $i++) 
        {
            $randUtil = rand(1,10);
            \App\Models\Image::create([
                'title' => 'animalFerme'.$i,
                'description' => $faker->sentence(10,true),
                'user_id'=>$randUtil,
                'id_genre'=>5,
                'source' => '/assets/img/contenu_site/animalFerme'.$i.'.jpg',
                'date_creation' => date("05-05-22")
            ]);
        }

    }
}
