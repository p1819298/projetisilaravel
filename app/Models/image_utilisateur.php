<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class image_utilisateur extends Model
{
    use HasFactory;

    public static function existeLine($utilisateur_id, $image_id)
    {    
        $utilisateur = User::find($utilisateur_id);
        
        //On vérifie s'il a liké l'image
        $utilisateur->with('imagesAimees')->get();
        $images_liked = $utilisateur->imagesAimees;
        
        foreach($images_liked as $image_liked)
        {
            //echo $image_liked['id'];
            if($image_liked['id'] == $image_id)
            {
                return true;
            }
        }

        return false;
    }

    public static function addImageUtilisateur($utilisateur_id, $image_id)
    {
        $utilisateur = User::findOrFail($utilisateur_id);
        $image = Image::findOrFail($image_id);
        $image->utilisateurs()->attach($utilisateur);
    }

    public static function destroyImageUtilisateur($utilisateur_id, $image_id)
    {
        $utilisateur = User::findOrFail($utilisateur_id);
        $image = Image::findOrFail($image_id);
        $image->utilisateurs()->detach($utilisateur);
    }
}
