<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InsertImageRequest extends FormRequest
{
    protected $fillable = ["title", "description", "utilisateur_id", "id_genre", "source"];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'max:150'],
            'description' => ['required', 'string', 'max:400'],
            'source' =>['required','mimes:jpeg,jpg,png','max:1000']
        ];
    }
}
