<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;

class SearchImageController extends Controller
{
    public function postInfos(Request $request)
    {
        /*echo 'message = ';
        echo $request->input('search_image');*/
        $value_search = $request->input('search_image');

        //$images = Image::where('title', 'reptile8')->get();
        $images = Image::where('title', 'like', $value_search.'%')->orWhere('title', 'like',$value_search.'%')->get();
        return view('homeConnected', compact('images'));
    }

    public function getInfos()
    {
        return redirect('images');
    }

}
